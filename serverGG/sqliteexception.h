#ifndef SQLITEEXCEPTION_H
#define SQLITEEXCEPTION_H

#include <exception.h>
#include <sqlite3.h>
class SqliteException : public Exception
{
public:
    SqliteException(int errorNumber, std::string message);
    virtual std::string getSystemErrorMessage();
};

#endif // SQLITEEXCEPTION_H
