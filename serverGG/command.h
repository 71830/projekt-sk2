#ifndef COMMAND_H
#define COMMAND_H

#include <commandenum.h>
class Command
{
private:
      CommandType type;
      const char * message;
      const char * from;
      const char * to;

      //from
      //to



public:
    Command(const char * message, int commandType, const char * from, const char * to);
    Command(){;}
    ~Command();

    bool validate();

    CommandType getType();

    const char * getMessage();
    const char * getFrom();
    const char * getTo();



};

#endif // COMMAND_H
