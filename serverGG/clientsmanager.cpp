#include "clientsmanager.h"
#include <string.h>
ClientsManager::ClientsManager()
{
    this->clients = new list<ServerClient*>();
}

ClientsManager::~ClientsManager(){
    delete this->clients;
}

void ClientsManager::add(ServerClient *client){
    this->clients->push_back(client);
}

bool ClientsManager::remove(ServerClient * client){ //change it
    this->clients->remove(client);
    return true;
}

int ClientsManager::getClientsCount(){
    return this->clients->size();
}

ServerClient * ClientsManager::getClientByName(string username){
    for (std::list<ServerClient*>::iterator it=clients->begin(); it != clients->end(); ++it){

        ServerClient * client = *it;

        if(username.compare(client->getUsername())== 0){
            return *it;
        }
    }
    return nullptr;
}

ServerClient * ClientsManager::getClientByFd(int fd){
    for (std::list<ServerClient*>::iterator it=clients->begin(); it != clients->end(); ++it){
        if((*it)->getSocket()->getDescriptor() ==  fd){
            return *it;
        }
    }
    return nullptr;
}

list<ServerClient*> * ClientsManager::getClients(){
    return this->clients;
}
