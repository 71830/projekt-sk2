#include "sqlitecontroller.h"
#include <sqliteexception.h>
#include <sys/stat.h>
SqliteController::SqliteController()
{
    bool exists = true;
    if(!this->databaseExists("servergg.db")){
        std::cout<<"Database does not exists. Creating new one.\n";
        exists = false;
    }
    int error;
    if((error = sqlite3_open("servergg.db", &db)) != SQLITE_OK){
        throw SqliteException(error, "Cannot open database.");
    } else {
       std::cout<<"Sqlite: Data base has been opened\n";

       if(!exists){
           char * query = "CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    "name varchar(20) not null unique, password varchar(10) not null);";
           char * errmsg;
           int creatingError = sqlite3_exec(getDB(), query, NULL, 0, &errmsg);
           if(creatingError < 0 ){
               throw SqliteException(creatingError, errmsg);
           } else {
               std::cout<<"Sqlite: User table created successfully!\n";
           }
       }
    }
}

void SqliteController::insertNewUser(std::string &name, std::string &password){
    char * errmsg;
    int error = sqlite3_exec(getDB(), ("insert into users (name, password) values('" + name + "','" + password + "');").c_str(), 0, 0, &errmsg);
    if(error != SQLITE_OK){
        throw SqliteException(error, errmsg);
    } else {
        std::cout<<"Sqlite: User "<<name<<" has been added!\n";
    }
}

bool SqliteController::userExist(std::string &name){
    char * errmsg;
    char ** res;
    int row, col;
    int error = sqlite3_get_table(getDB(), ("select id from users where name='"+name+"';").c_str(), &res, &row, &col, &errmsg);
    if(error != SQLITE_OK){
        throw SqliteException(error, errmsg);
    }
    sqlite3_free_table(res);
    return row > 0;
}

bool SqliteController::checkCredentials(std::string &name, std::string &password){
    char * errmsg;
    char ** res;
    int row, col;
    int error = sqlite3_get_table(getDB(), ("select id from users where name='" + name + "' and password='" + password + "';").c_str(), &res, &row, &col, &errmsg);
    if(error != SQLITE_OK){
        throw SqliteException(error, errmsg);
    }
    sqlite3_free_table(res);
    return row > 0;
}


SqliteController::~SqliteController(){
    if(db != nullptr){
        sqlite3_close(db);
    }
}

SqliteController& SqliteController::getInstance()
{
    static SqliteController controll;
    return controll;
}

sqlite3* SqliteController::getDB(){
    return this->db;
}

bool SqliteController::databaseExists(const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}
