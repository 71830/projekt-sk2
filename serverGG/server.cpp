#include "server.h"
#include "serverclient.h"
#include "unistd.h"
#include <statics.h>
#include <sqlitecontroller.h>

Server::Server(char * serverIp, int port, int listenQueueSize)
{
    SqliteController::getInstance();


    if(listenQueueSize <= 0){
        throw ServerException(0, "Listen queue size must be greater than 0.");
    }

    try{
        this->socket = new Socket(serverIp, port);
    } catch(Exception & ex){
        throw ServerException(ex.getErrorNumber(), ex.getErrorMessage());

    }

    this->clientsManager = new ClientsManager();
    this->acceptLoopCanRun = true;

}

Server::~Server(){
    delete this->socket;
    delete this->clientsManager;
}

void Server::run()
{
   this->bindServer();
   this->startListen();
   this->acceptLoop();
}

void Server::bindServer()
{
    int bindResult = bind(this->getSocket()->getDescriptor(),(sockaddr*) this->getSocket()->getSocketAddress(), sizeof(*this->getSocket()->getSocketAddress()));

    if(bindResult < 0)
    {
        throw ServerException(errno, "Server cannot bind to given ip and port.");
    }
}

void Server::startListen()
{
    int listenResult = listen(this->getSocket()->getDescriptor(), this->listenQueueSize);

    if(listenResult < 0)
    {
        throw ServerException(errno, "Server cannot start listening.");
    }
}

void Server::acceptLoop(){ 

    this->createNewEpollAndAddAcceptEvent();

    epoll_event e;

    while(this->acceptLoopCanRun && this->getEventPoll()->wait(&e, 1, -1)){
        if(e.data.fd == this->getSocket()->getDescriptor()){
            this->acceptClient();
        } else if (getClientsMenager()->getClientByFd(e.data.fd) != nullptr){
            if(e.events & EPOLLRDHUP){
                removeClient(getClientsMenager()->getClientByFd(e.data.fd));
                //send message to all that user is not active anymore;
            }
            else if(e.events == EPOLLIN){
                Command cmd = readCommand(e.data.fd);
                executeCommand(cmd);

            }
        }

    }

    this->getSocket()->closeSocket();
}

Command Server::readCommand(int fd){
    int commandType = readCommandType(fd);
    char * messageBuf = readMessage(fd, MessageBufSize);

    char * from = readUserName(fd);
    char * to = readUserName(fd);

    return Command(messageBuf, commandType, from, to);
}


void Server::sendBroadcastMessage(Command &cmd){
 for(list<ServerClient*>::iterator it= getClientsMenager()->getClients()->begin(); it != getClientsMenager()->getClients()->end();++it){
    sendMessage((*it)->getSocket()->getDescriptor(), cmd);
 }
}


void Server::sendMessage(int fd, Command &cmd){

    write(fd, std::to_string(cmd.getType()).c_str(), STATICS::CommandTypeSize);
    write(fd, std::to_string(strlen(cmd.getMessage())).c_str(), STATICS::MessageBufSize);
    write(fd, cmd.getMessage(), strlen(cmd.getMessage()));
    write(fd, cmd.getFrom(), STATICS::UserNameBufSize);
    write(fd, cmd.getTo(), STATICS::UserNameBufSize);

}

bool Server::executeCommand(Command &cmd){
    if(!cmd.validate()) return false;

    switch (cmd.getType()) {
    case CommandType::REGISTER:
        {
            ServerClient* client = getClientsMenager()->getClientByName(cmd.getFrom());
            if(client != nullptr){
                try{
                    std::string name(cmd.getFrom());
                    std::string pass(cmd.getMessage());

                    SqliteController::getInstance().insertNewUser(name, pass);
                    Command servCmd = Command("Success", CommandType::REGISTER_SUCCESS, "server", cmd.getFrom());
                    sendMessage(client->getSocket()->getDescriptor(), servCmd);
                } catch (Exception &ex){
                    Command servCmd = Command(ex.getSystemErrorMessage().c_str(), CommandType::REGISTER_FAILED, "server", cmd.getFrom());
                    sendMessage(client->getSocket()->getDescriptor(), servCmd);
                }
            }
            break;
        }
    case CommandType::LOG_IN:
        {
            ServerClient* client = getClientsMenager()->getClientByName(cmd.getFrom());
            Command servCmd;
            if(client != nullptr){
                try{
                    std::string name(cmd.getFrom());
                    std::string pass(cmd.getMessage());

                    if(SqliteController::getInstance().checkCredentials(name, pass)){
                        servCmd = Command("Success", CommandType::LOGGING_SUCCESS, "server", cmd.getFrom());
                        client->setLogged(true);
                        std::cout<<"Server: Logging: User: "<<cmd.getFrom()<<" has been logged in.\n";

                    } else {
                        servCmd = Command("Failed. User does not exists or wrong password.", CommandType::LOGGING_FAILED, "server", cmd.getFrom());
                        std::cout<<"Server: Logging: Bad login request from descriptor: "<<client->getSocket()->getDescriptor()<<" with user: "<<cmd.getFrom()<<endl;

                    }
                } catch (Exception &ex){
                    servCmd = Command("Failed. Database problem found. Try again.", CommandType::LOGGING_FAILED, "server", cmd.getFrom());
                    std::cout<<"Server: Logging: Database bad login request from descriptor: "<<client->getSocket()->getDescriptor()<<" with user: "<<cmd.getFrom()<<endl;
                }
                sendMessage(client->getSocket()->getDescriptor(), servCmd);
            }
            break;
        }
    case CommandType::SEND_MESSAGE:
        {
            ServerClient* client = getClientsMenager()->getClientByName(cmd.getTo());
            if(client != nullptr){
                sendMessage(client->getSocket()->getDescriptor(), cmd);
            }
            break;
        }
    default:
        break;
    }
}



char * Server::readUserName(int fd){
    char * user = new char[UserNameBufSize];
    int readc = read(fd, user, UserNameBufSize);
    if(readc != UserNameBufSize)
        return nullptr;
    return user;
}

int Server::readCommandType(int fd){

    char commandTypeBuf[CommandTypeSize];

    int readc = read(fd, commandTypeBuf, CommandTypeSize);
    if(readc != CommandTypeSize)
        return -1;
    return atoi(commandTypeBuf);

}


char * Server::readMessage(int fd, int sizeOfFirstRead){

    char temp[sizeOfFirstRead];
    int readc = read(fd, temp, sizeOfFirstRead);
    if(readc > 0){
        int size = atoi(temp);

        char * buffer = new  char[2];
        readc = read(fd, buffer, size);
        char * x = strerror(errno);
        write(1, x, strlen(x));
        if(readc > 0){
            return buffer;
        }
    }
    return nullptr;

}

void Server::acceptClient(){
    sockaddr_in clientSocket;
    socklen_t socketSize;

    int acceptValue = accept(this->getSocket()->getDescriptor(), (sockaddr*)&clientSocket, &socketSize);
    if(acceptValue > 0){
        this->addNewClient(acceptValue, &clientSocket);
    }
}

void Server::createNewEpollAndAddAcceptEvent(){
    try{
        this->eventPoll = new NewEpoll(0);
        this->getEventPoll()->addEvent(this->getSocket()->getDescriptor(), EPOLLIN);
    } catch (Exception & ex){
        throw ServerException(ex.getErrorNumber(), ex.getErrorMessage());
    }
}

void Server::addNewClient(int acceptValue, sockaddr_in * sockAddr){
    try{
       this->getEventPoll()->addEvent(acceptValue, EPOLLIN | EPOLLRDHUP);
       try{
            ServerClient * client = new ServerClient(new Socket(acceptValue, *sockAddr));
            std::string user = std::to_string(acceptValue);
            client->setUsername(user);

            this->getClientsMenager()->add(client);
        }catch(Exception & ex){
            this->getEventPoll()->removeEvent(acceptValue);
            cout<<"[Log][Error] "<<ex.getErrorMessage();
        }
    } catch (Exception  &ex){
        cout<<"[Log][Error] "<<ex.getErrorMessage();
    }
}

void Server::removeClient(ServerClient * client){
    this->getEventPoll()->removeEvent(client->getSocket()->getDescriptor());
    cout<<"Client "<<client->getUsername()<<" removed.\n";
    this->getClientsMenager()->remove(client);
}

Socket * Server::getSocket(){
    return this->socket;
}


ClientsManager * Server::getClientsMenager(){
    return this->clientsManager;
}

NewEpoll * Server::getEventPoll(){
    return this->eventPoll;
}
