#ifndef EXCEPTION_H
#define EXCEPTION_H

#include<string.h>
#include<string>
class Exception
{
private:
    int errorNumber;
    std::string errorMessage;

public:
    Exception();
    Exception(int errorNumber, std::string errorMessage);

    int virtual getErrorNumber();
    std::string virtual getErrorMessage();
    std::string virtual getSystemErrorMessage();

};



#endif // EXCEPTION_H
