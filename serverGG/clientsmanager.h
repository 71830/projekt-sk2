#ifndef CLIENTSMANAGER_H
#define CLIENTSMANAGER_H
#include <serverclient.h>
#include <list>
#include <sys/epoll.h>
#include <thread>
#include <errno.h>
#include <iostream>
#include <string>
using namespace std;
class ClientsManager
{
private:
    list<ServerClient*> * clients;

    void epollLoop();
public:
    ClientsManager();
    ~ClientsManager();

    void add(ServerClient * client);
    bool remove(ServerClient * client);
    int getClientsCount();
    ServerClient * getClientByName(string username);
    ServerClient * getClientByFd(int fd);
    list<ServerClient*> *getClients();
};

#endif // CLIENTSMANAGER_H
