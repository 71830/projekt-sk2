#include "socket.h"

Socket::Socket(int socketDescriptor, sockaddr_in socketAddress)
{
    this->socketDesc = socketDescriptor;
    this->socketAddress = socketAddress;
}


Socket::Socket(char * ip, int port){

    if(port <= 0){
        throw Exception(0, "Port must be greater than 0.");
    }

    inet_aton(ip, &this->socketAddress.sin_addr);
    this->socketAddress.sin_family = AF_INET;
    this->socketAddress.sin_port = htons(port);

    this->socketDesc = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if(this->socketDesc < 0){
        throw Exception(errno, "Cannot create socket.");
    }

}

void Socket::closeSocket(){
    close(this->getDescriptor());
}

int Socket::getDescriptor(){
    return this->socketDesc;
}

sockaddr_in * Socket::getSocketAddress(){
    return &this->socketAddress;
}

char * Socket::getIp(){
    return inet_ntoa(this->socketAddress.sin_addr);
}

int Socket::getPort(){
    return ntohs(this->getSocketAddress()->sin_port);
}

