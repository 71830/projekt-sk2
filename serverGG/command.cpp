#include "command.h"
#include <exception.h>
Command::Command(const char * message, int commandType, const char * from, const char * to)
{
    this->message = message;
    this->type = (CommandType)commandType;
    this->from = from;
    this->to = to;
}


bool Command::validate(){
    return !(this->getType() == -1 || this->getMessage() == nullptr || this->getFrom() == nullptr || this->getTo() == nullptr);
}


CommandType Command::getType(){
    return this->type;
}

const char * Command::getMessage(){
    return this->message;
}

const char * Command::getFrom(){
    return this->from;
}

const char * Command::getTo(){
    return this->to;
}


Command::~Command(){
    delete message;
    delete from;
    delete to;
}
