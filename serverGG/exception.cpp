#include "exception.h"

Exception::Exception()
{
    this->errorNumber = 0;
    this->errorMessage = "";
}

Exception::Exception(int errorNumber, std::string errorMessage)
{
    this->errorNumber = errorNumber;
    this->errorMessage = errorMessage;
}

int Exception::getErrorNumber()
{
    return this->errorNumber;
}

std::string Exception::getErrorMessage()
{
    return this->errorMessage;
}

std::string Exception::getSystemErrorMessage()
{
    return strerror(this->errorNumber);
}
